import React from 'react';
import { useState, useEffect } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Image,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { removeFromItemList, updateData } from './utils/actions';

const DeleteMultiple = (props) => {
    let [originallist, setOriginalList] = useState([])
    let [newItemList, setNewItemList] = useState([])
    let [deleteList, setDeleteList] = useState([])
    const [selectedId, setSelectedId] = useState(null);

    useEffect(() => {
        loadList()
    },
        []);

    const loadList = () => {
        let original = props.itemlist.map(item => {

            item = {
                id: item,
                name: item,
                isSelect: false
            }
            return item;
        });
        setOriginalList(original)
        // Data saved in original list with isSelect value 
    };

    const selectedItems = (item) => {
        setSelectedId(item)

        if (item.isSelect) {
            item.isSelect = false;
        } else {
            item.isSelect = true;
        }
        const index = originallist.findIndex(itemlist =>
            itemlist.id === item.id
        );
        originallist[index] = item;
        setOriginalList(originallist)

        // selected Items added in deletelist for delete
        setNewItemList(props.itemlist)
        if (deleteList.includes(item.id)) {
            let removeitem = deleteList.filter(itemlist =>
                itemlist != item.id
            )
            setDeleteList(removeitem)
        }
        else {
            let delItem = [...deleteList, item.id]
            setDeleteList(delItem)
        }
        // console.log('delete list :' + JSON.stringify(deleteList))
    };

    //  Delete Seleted Items from store list
    const deleteSelectedItem = () => {
        if (deleteList.length == 0) {
            alert("No Items Selected")
        }
        else {
            newItemList = newItemList.filter(item => {
                return deleteList.indexOf(item) === -1
            })
            props.updateData(newItemList);
            Alert.alert(
                "Items Deleted Successfully", "",
                [{
                    text: "ok",
                    onPress: () => { props.navigation.goBack() }
                },
                ])
            setDeleteList([])
        }
    };

    const renderItem = ({ item }) => {
        const backgroundColor = item.isSelect ? "#98dd99" : "#fff";
        return (
            <View>
                <TouchableOpacity
                    onPress={() => selectedItems(item)}
                >
                    <View style={[styles.listItemStyle, { backgroundColor }]}>

                        <View style={{ flexDirection: 'row' }}>

                            <Image style={styles.image}
                                source={require('/home/intel/Desktop/RaviTraining/ReduxExample/src/images/user.png')}
                            />
                            <Text style={{
                                fontSize: 18,
                                padding: 5,
                                fontWeight: 'bold',
                                color: '#666'
                            }}>
                                {item.id}
                            </Text>
                        </View>

                    </View>
                </TouchableOpacity>

            </View>

        );
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>

                <View style={{ margin: 10, flex: 1 }}>
                    <FlatList
                        data={originallist}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                        extraData={selectedId}
                    />
                </View>
                <TouchableOpacity>
                    <Text style={styles.buttonDelete}
                        onPress={() => deleteSelectedItem()}
                    >
                        Delete Items: ({deleteList.length})
                </Text>

                </TouchableOpacity>

            </View>

        </SafeAreaView >

    );
};
const mapStateToProps = (state) => {
    const { itemlist } = state;
    return {
        itemlist: itemlist,
    };
};

const mapDispatchToProps = (dispatch) => ({
    updateData: (newItemList) => dispatch(updateData(newItemList))
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteMultiple);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 5,
    },
    listItemStyle: {
        margin: 5,
        padding: 5,
        borderWidth: .5,
        borderRadius: 5,
        borderColor: '#aaa',
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    image: {
        margin: 5,
        height: 20,
        width: 20,
        margin: 5,
    },
    buttonDelete: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        backgroundColor: 'red',
        margin: 10,
        borderRadius: 10,
        fontWeight: 'bold'
    },
});