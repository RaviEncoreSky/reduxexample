import React from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    FlatList,
    Image,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { removeFromItemList, updateData } from './utils/actions';

const Home = (props) => {

    const deleteItem = (item) => {
        let filteredItems = props.itemlist.filter(itemlist => itemlist != item)
        props.updateData(filteredItems);

    };

    const renderItem = ({ item }) => {

        return (
            <TouchableOpacity>
                <View style={styles.listItemStyle}>

                    <View style={{ flexDirection: 'row' }}>

                        <Image style={styles.image}
                            source={require('/home/intel/Desktop/RaviTraining/ReduxExample/src/images/user.png')}
                        />
                        <Text style={{ fontSize: 18, padding: 5, fontWeight: 'bold', color: '#666' }}>
                            {item}
                        </Text>

                    </View>

                    <View style={{ flexDirection: 'row' }}>

                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('EditList',
                                {
                                    user: { itemValue: item }
                                }
                            )}
                        >
                            <Image style={styles.image}
                                source={require('/home/intel/Desktop/RaviTraining/ReduxExample/src/images/img_edit.png')}
                            />

                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() =>
                                Alert.alert(
                                    "Do you want to delete this user ?", "",
                                    [{
                                        text: "OK",
                                        onPress: () => deleteItem(item)
                                    },
                                    {
                                        text: "Cancel",
                                        onPress: () => { console.log("Cancel Pressed") }
                                    }]
                                )}>

                            <Image style={styles.image}
                                source={require('/home/intel/Desktop/RaviTraining/ReduxExample/src/images/img_delete.png')}
                            />

                        </TouchableOpacity>

                    </View>

                </View>
            </TouchableOpacity>

        );
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>

                <View style={{ margin: 10, flex: 1 }}>

                    <View style={styles.textContainer}>

                        <TouchableOpacity
                            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                            onPress={() => props.navigation.navigate('DeleteMultiple')}>

                            <Text style={styles.titleStyle}>
                                Delete Multiple items
                            </Text>

                            <Image
                                style={{ height: 15, width: 15, alignSelf: 'center' }}
                                source={require('/home/intel/Desktop/RaviTraining/ReduxExample/src/images/arrow.png')}
                            />

                        </TouchableOpacity>

                    </View>

                    <FlatList
                        data={props.itemlist}
                        renderItem={renderItem}
                        // keyExtractor={(_item, index) => index.toString()}
                        keyExtractor={item => item.id}
                    />

                </View>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('AddNewUser')}
                    style={styles.touchableOpacityStyle}>
                    <Image
                        source={require('/home/intel/Desktop/RaviTraining/ReduxExample/src/images/plus_icon.png')}
                        style={styles.floatingButtonStyle}
                    />
                </TouchableOpacity>

            </View>

        </SafeAreaView >

    );
};

const mapStateToProps = (state) => {
    const { itemlist } = state;
    return {
        itemlist: itemlist,
    };
};

const mapDispatchToProps = (dispatch) => ({
    removeFromItemList: (item) => dispatch(removeFromItemList(item)),
    updateData: (filteredItems) => dispatch(updateData(filteredItems))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 5,
    },
    titleStyle: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        backgroundColor: '#129685',
        color: '#fff'
    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        backgroundColor: '#98dd99',
        margin: 10,
        borderRadius: 10,
        fontWeight: 'bold'
    },
    listItemStyle: {
        margin: 5,
        padding: 5,
        borderWidth: .5,
        borderRadius: 5,
        borderColor: '#aaa',
        flexDirection: 'row',
        justifyContent: "space-between"

    },
    image: {
        margin: 5,
        height: 20,
        width: 20,
        margin: 5,
    },
    touchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },
    floatingButtonStyle: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
    },

    title: {
        fontSize: 24,
        margin: 5,
        padding: 5
    },
    textContainer: {
        margin: 3,
        color: '#1a5670',
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#129685',
    },

});