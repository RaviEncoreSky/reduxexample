import React from 'react';

import { useState, useEffect } from 'react';
import {
    TextInput,
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { updateData } from './utils/actions';

const AddNewUser = (props) => {
    const [userName, setUserName] = useState('');

    const addNewUser = (userName) => {
        if (userName != '') {
            let newItems = [...props.itemlist, userName]
            props.updateData(newItems);

            Alert.alert(
                "User Added Successfully", "",
                [{
                    text: "ok",
                    onPress: () => { props.navigation.goBack() }
                },
                ])

        }
        else { alert('Please Enter new name') }
    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>

                <TextInput style={styles.textInputStyle}
                    placeholder='Enter Name'

                    onChangeText={userName => setUserName(userName)}>
                </TextInput>

                <TouchableOpacity
                    onPress={() => addNewUser(userName)}
                >
                    <Text style={styles.buttonStyle}>
                        Add User
                        </Text>

                </TouchableOpacity>

            </View>

        </SafeAreaView>
    );
};

const mapStateToProps = (state) => {
    const { itemlist } = state;
    return {
        itemlist: itemlist
    };
};

const mapDispatchToProps = (dispatch) => ({
    updateData: (newItemList) => dispatch(updateData(newItemList))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewUser);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,

    },
    textInputStyle: {
        fontSize: 14,
        padding: 5,
        borderWidth: .5,
        margin: 10,
        marginTop: 10,
        borderRadius: 5,
        borderColor: '#aaa'
    },
    buttonStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        backgroundColor: '#98dd99',
        margin: 10,
        borderRadius: 10,
        fontWeight: 'bold'
    },

});