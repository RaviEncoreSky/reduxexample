

export const saveDetails = (userName, cityName) => {
    return (dispatch) => {
        dispatch({
            type: 'SAVE_DETAILS',
            payload: userName, cityName,
        });
    };
};

export const removeFromItemList = (filteredItems) => {
    return (dispatch) => {
        dispatch({
            type: 'UPDATE_ARRAY',
            payload: filteredItems,
        });
    };
};

export const updateData = (newItemList) => {
    return (dispatch) => {
        dispatch({
            type: 'UPDATE_ARRAY',
            payload: newItemList,
        });
    };
};

