
const initialState = {
    name: '',
    city: '',
    itemlist: [],
};

const reducers = (state = initialState, action) => {
    switch (action.type) {

        case 'SAVE_DETAILS':
            return state,
            {
                name: action.payload,
                city: action.cityName,
            };

        case 'UPDATE_ARRAY':
            return state, {
                itemlist: action.payload
            };

        default:
            return state;
    }
};

export default reducers;