import React from 'react';
import { useState, useEffect } from 'react';
import {
    TextInput,
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { updateData } from './utils/actions';

const EditList = (props) => {

    const [newUserName, setNewUserName] = useState('');
    let [newItemList, setNewItemList] = useState([])
    const { user } = props.route.params;
    let index = props.itemlist.indexOf(user.itemValue)

    useEffect(() => {
        setNewItemList(props.itemlist)
    },
        []);

    const updateName = (newUserName) => {
        if (newUserName != '') {
            newItemList[index] = newUserName
            props.updateData(newItemList);
            Alert.alert(
                "Updates Successfully", "",
                [{
                    text: "ok",
                    onPress: () => { props.navigation.replace('UserHome') }

                },
                ])

        }
        else { alert('Please Enter new name') }

    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>

                <View style={styles.titleText}>

                    <Text style={styles.textTitleStyle}>
                        Original Name :
                </Text>
                    <Text style={styles.textTitleStyle}>
                        {user.itemValue}
                    </Text>
                </View>

                <Text style={styles.textStyle}>
                    New Name
                </Text>

                <TextInput style={styles.textInputStyle}
                    placeholder='Enter New Name'
                    onChangeText={newUserName => setNewUserName(newUserName)}>
                </TextInput>

                <TouchableOpacity
                    onPress={() => updateName(newUserName)}
                >
                    <Text style={styles.buttonStyle}>
                        UPDATE
                        </Text>

                </TouchableOpacity>

            </View>

        </SafeAreaView>
    );
};

const mapStateToProps = (state) => {
    const { itemlist } = state;
    return {
        itemlist: itemlist
    };
};

const mapDispatchToProps = (dispatch) => ({
    updateData: (newItemList) => dispatch(updateData(newItemList))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditList);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,

    },
    textStyle: {
        fontSize: 12,
        paddingTop: 5,
        marginLeft: 10,
        marginTop: 15,
        fontWeight: 'bold',
        color: "#666"
    },
    textTitleStyle: {
        fontSize: 18,
        padding: 5,
        fontWeight: 'bold',
        color: '#fff',
        marginLeft: 5

    },
    titleText: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        margin: 10,
        backgroundColor: '#129685',
        padding: 5,
        borderRadius: 10
    },

    textInputStyle: {
        fontSize: 14,
        padding: 5,
        borderWidth: .5,
        margin: 10,
        borderRadius: 5,
        borderColor: '#aaa'
    },
    buttonStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        backgroundColor: '#98dd99',
        margin: 10,
        borderRadius: 10,
        fontWeight: 'bold'
    },
});