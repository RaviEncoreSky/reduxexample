import React from 'react';

import { useState, useEffect } from 'react';
import {
    TextInput,
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { saveDetails } from './utils/actions';

const Edit = (props) => {

    const [userName, setUserName] = useState('');
    const [cityName, setCityName] = useState('');

    useEffect(() => {
        loadData()
    },
        []);

    const loadData = () => {
        console.log(props.name + "  " + props.city)
        setCityName(props.city)
        setUserName(props.name)
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>

                <TextInput style={styles.textInputStyle}
                    placeholder='Enter Name'
                    value={userName}
                    onChangeText={userName => setUserName(userName)}>
                </TextInput>

                <TextInput style={styles.textInputStyle}
                    placeholder='Enter City'
                    value={cityName}
                    onChangeText={cityName => setCityName(cityName)}>
                </TextInput>

                <TouchableOpacity
                    onPress={() => { props.saveDetails(userName, cityName); }}>
                    <Text style={styles.textStyle}>
                        Edit and Save
                        </Text>

                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => { props.navigation.goBack() }}>

                    <Text style={styles.textStyle}>
                        Back
                        </Text>

                </TouchableOpacity>

            </View>

        </SafeAreaView>
    );
};

const mapStateToProps = (state) => {
    const { name, city, } = state;
    return {
        name: name,
        city: city,
    };
};

const mapDispatchToProps = (dispatch) => ({
    saveDetails: (userName, cityName) => dispatch(saveDetails(userName, cityName)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Edit);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        backgroundColor: '#98dd99',
        margin: 10,
        borderRadius: 10,
        fontWeight: 'bold'
    },

    textInputStyle: {
        fontSize: 14,
        padding: 5,
        borderWidth: .5,
        margin: 10,
        borderRadius: 5,
    },

});