import React from 'react';
import { useState, } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { saveDetails } from './utils/actions';

const Home = (props) => {
    const [userName, setUserName] = useState('');
    const [cityName, setCityName] = useState('');

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <Text style={styles.titleStyle}>
                    Enter Details
                    </Text>

                <TextInput style={styles.textInputStyle}
                    placeholder='Enter Name'
                    onChangeText={userName => setUserName(userName)}
                >
                </TextInput>

                <TextInput style={styles.textInputStyle}
                    placeholder='Enter City'
                    onChangeText={cityName => setCityName(cityName)}>
                </TextInput>

                <TouchableOpacity
                    onPress={() => { props.saveDetails(userName, cityName); }}
                >
                    <Text style={styles.textStyle}>
                        SAVE
                    </Text>

                </TouchableOpacity>

                <View style={{ borderWidth: .5, borderRadius: 5, marginTop: 30 }}>

                    <Text style={styles.titleStyle}>
                        Saved Details
                    </Text>

                    <Text
                        style={{ textAlign: 'center', margin: 10 }}>
                        Name : {props.name}
                    </Text>

                    <Text style={{ textAlign: 'center', margin: 10 }}>
                        City : {props.city}
                    </Text>

                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('Edit')}>

                        <Text style={styles.textStyle}>
                            Edit Details
                        </Text>

                    </TouchableOpacity>

                </View>

            </View>

        </SafeAreaView >

    );
};

const mapStateToProps = (state) => {
    const { name, city, } = state;
    return {
        city: city,
        name: name,
    };
};

const mapDispatchToProps = (dispatch) => ({
    saveDetails: (userName, cityName) => dispatch(saveDetails(userName, cityName)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    },
    titleStyle: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 10,

    },
    textStyle: {
        fontSize: 16,
        textAlign: 'center',
        padding: 10,
        backgroundColor: '#98dd99',
        margin: 10,
        borderRadius: 10,
        fontWeight: 'bold'
    },

    textInputStyle: {
        fontSize: 14,
        padding: 5,
        borderWidth: .5,
        margin: 10,
        borderRadius: 5,
    },

});