import 'react-native-gesture-handler';

import * as React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import rootReducer from './src/utils/reducers';
import Home from './src/Home';
import Edit from './src/Edit';
import EditList from './src/EditList';
import AddNewUser from './src/AddNewUser';
import UserHome from './src/UserHome';
import DeleteMultiple from './src/DeleteMultiple';

const store = createStore(rootReducer, applyMiddleware(thunk));

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="UserHome">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Edit" component={Edit} />
        {/* <Stack.Screen name="AddUser" component={AddUser} /> */}
        <Stack.Screen name="UserHome" component={UserHome}
          options={{
            headerTitle: 'Home'
          }}
        />
        <Stack.Screen name="EditList" component={EditList}
          options={{
            headerTitle: 'Edit List'
          }}
        />
        <Stack.Screen name="AddNewUser" component={AddNewUser}
          options={{
            headerTitle: 'Add New User'
          }}
        />

        <Stack.Screen name="DeleteMultiple" component={DeleteMultiple}
          options={{
            headerTitle: 'Delete Items'
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <AppNavigator />
    </Provider>
  );
};

export default App;